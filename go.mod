module gitlab.com/juhani/go-semrel-gitlab

require (
	github.com/blang/semver v3.5.1+incompatible
	github.com/gliderlabs/ssh v0.1.3 // indirect
	github.com/golang/protobuf v1.3.0 // indirect
	github.com/juranki/go-semrel v0.0.0-20190119081135-30298f800350
	github.com/pkg/errors v0.8.1
	github.com/urfave/cli v1.20.0
	github.com/xanzy/go-gitlab v0.16.0
	golang.org/x/crypto v0.0.0-20190228161510-8dd112bcdc25 // indirect
	golang.org/x/net v0.0.0-20190301231341-16b79f2e4e95 // indirect
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421 // indirect
	golang.org/x/sync v0.0.0-20190227155943-e225da77a7e6 // indirect
	golang.org/x/sys v0.0.0-20190307162637-572b51eaf722 // indirect
	golang.org/x/text v0.3.1-0.20180807135948-17ff2d5776d2 // indirect
	gopkg.in/src-d/go-git.v4 v4.8.1
)
